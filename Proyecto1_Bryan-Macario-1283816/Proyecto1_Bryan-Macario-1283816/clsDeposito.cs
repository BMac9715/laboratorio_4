﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_Bryan_Macario_1283816
{
    class clsDeposito
    {
        private string _Nombre;
        private double _Precio;
        private double _Costo;
        private double _CostoIngresado;

        // Constructor 
        public clsDeposito (string Nombre, double Precio, double Costo)
        {
            _Nombre = Nombre;
            _Precio = Precio;
            _Costo = Costo;
           
        }

        //Constructor vacio
        public clsDeposito()
        {
        }

        public string Nombre
        {
            get
            {
                return _Nombre; 
            }
            set
            {
                _Nombre = value;
            }
        }

        public double Precio
        {
            get
            {
                return _Precio;
            }
            set
            {
                _Precio = value;
            }
        }

        public double Costo
        {
            get
            {
                return _Costo;
            }
            set
            {
                _Costo = value;
            }
        }

        public double CostoIngresado
        {
            get
            {
                return _CostoIngresado;
            }
            set
            {
                _CostoIngresado = value;
            }
        }

     }

}

