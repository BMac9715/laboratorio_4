﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_Bryan_Macario_1283816
{
    class clsIngresarGasolina
    {
        private int _CantidadCombustible;
        private int _CantidadDeposito;
        private int _CantidadIngresar;


        //Constructor 
        public clsIngresarGasolina(int CantidadCombustible, int CantidadDeposito, int CantidadIngresar)
        {
            _CantidadCombustible = CantidadCombustible;
            _CantidadDeposito = CantidadDeposito;
            _CantidadIngresar = CantidadIngresar;
        }

        public clsIngresarGasolina()
        {
        }

        public int CantidadCombustible
        {
            get
            {
                return _CantidadCombustible;
            }
            set
            {
                _CantidadCombustible = value;
            }
        }

        public int CantidadDeposito
        {
            get
            {
                return _CantidadDeposito;
            }
            set
            {
                _CantidadDeposito = value;
            }

        }

        public int CantidadIngresar
        {
            get
            {
                return _CantidadIngresar;
            }
            set
            {
                _CantidadIngresar = value;
            }

        }

        //FUNCIONES

        public int CantidadTotalDeposito()
        {
            int CTDeposito;
            CTDeposito = _CantidadCombustible;
            _CantidadDeposito = CTDeposito;
            return CTDeposito;
        }

    }
}
