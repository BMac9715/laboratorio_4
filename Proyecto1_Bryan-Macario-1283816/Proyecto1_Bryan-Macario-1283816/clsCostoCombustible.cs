﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_Bryan_Macario_1283816
{
    class clsCostoCombustible
    {
        private double _CostoAct;
        private double _CostoP;
        private double _CantP;
        private double _CantIngresada;
        private double _CostoIngresado;

        //Constructor 
        public clsCostoCombustible(double CostoP, double CantP, double CantIngresada, double CostoIngresado)
        {
            _CostoP = CostoP;
            _CantP = CantP;
            _CantIngresada = CantIngresada;
            _CostoIngresado = CostoIngresado;
        }
        //Constructor Vacío
        public clsCostoCombustible()
        {

        }

        public double CostoAct
        {
           get
            {
                return _CostoAct;
            }
            set
            {
                _CostoAct = value;
            }
        }

        public double CostoP
        {
            get
            {
                return _CostoP;
            }
            set
            {
                _CostoP = value;
            }
        }

        public double CantP
        {
            get
            {
                return _CantP;
            }
            set
            {
                _CantP = value;
            }
        }

        public double CantIngresada
        {
            get
            {
                return _CantIngresada; 
            }
            set
            {
                _CantIngresada = value;
            }
        }

        public double CostoIngresado
        {
            get
            {
                return _CostoIngresado;
            }
            set
            {
                _CostoIngresado = value;
            }
        }

        //FUNCIONES

        public double CostoActual()
        {
            double CostoA;
            CostoA = ((CantP * CostoP) + (CostoIngresado * CantIngresada)) / (CantP + CantIngresada);
            return CostoA;
        }
    }
}
