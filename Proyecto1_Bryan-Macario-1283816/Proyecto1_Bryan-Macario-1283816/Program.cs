﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto1_Bryan_Macario_1283816
{
    class Program
    {
        static void Main(string[] args)
        {

            int eleccion2;

            clsDeposito DDiesel = new clsDeposito("Diesel", 12.00, 10.00);
            clsDeposito DRegular = new clsDeposito("Regular", 15.00, 12.00);
            clsDeposito DSuper = new clsDeposito("Super", 18.00, 15.00);
            clsIngresarGasolina Diesel = new clsIngresarGasolina(10, 0, 0);
            clsIngresarGasolina Regular = new clsIngresarGasolina(10, 0,0);
            clsIngresarGasolina Super = new clsIngresarGasolina(10, 0, 0);
            clsCostoCombustible Diesel1 = new clsCostoCombustible(0, 0, 0, 0);
            clsCostoCombustible Regular1 = new clsCostoCombustible(0, 0, 0, 0);
            clsCostoCombustible Super1 = new clsCostoCombustible(0, 0, 0, 0);

            //Inicio del programa

            int elección;
            elección = 0;

        inicio:

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("         Bienvenido a Gasolineras Maca");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.WriteLine(" Seleccione la acción que desea realizar:");
            Console.WriteLine();
            Console.WriteLine("          ______________________________________________");
            Console.WriteLine("         | 1. Administración de Depositos              |");
            Console.WriteLine("         |_____________________________________________|");
            Console.WriteLine("         | 2. Venta de gasolina                        |");
            Console.WriteLine("         |_____________________________________________|");
            Console.WriteLine("         | 3. Información Financiera                   |");
            Console.WriteLine("         |_____________________________________________|");
            Console.WriteLine("         | 4. Manejo de datos                          |");
            Console.WriteLine("         |_____________________________________________|");
            Console.WriteLine();
            elección = Convert.ToInt32(Console.ReadLine());

            //MENU PRINCIPAL, ELEGIR DE 1 A 4 PARA INGRESAR A UNA OPCION

            switch (elección)
            {
                case 1:
                    Console.Clear();
                    goto Depositos;

                case 2:
                    Console.Clear();
                    goto Ventas;

                case 3:
                    Console.Clear();
                    goto Informacion;

                case 4:
                    Console.Clear();
                    goto Datos;

                default:
                    Console.WriteLine();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error. Esa opción no es valida. ");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.ReadLine();
                    Console.Clear();
                    goto inicio;

            }

        Depositos:
            //Muestra al operador la cantidad de combustible disponible en cada deposito
            intento:

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("             Administración de Depositos ");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine();
            Console.WriteLine("     _________________________________________________");
            Console.WriteLine("         Tipo de Combustible          Disponible");
            Console.WriteLine("     _________________________________________________");
            Console.WriteLine("             " + DDiesel.Nombre + "                    " + Diesel.CantidadTotalDeposito().ToString() + "/50 gal");
            Console.WriteLine("     _________________________________________________");
            Console.WriteLine("             " + DRegular.Nombre + "                   " + Regular.CantidadTotalDeposito().ToString() + "/50 gal");
            Console.WriteLine("     _________________________________________________");
            Console.WriteLine("             " + DSuper.Nombre + "                     " + Super.CantidadTotalDeposito().ToString() + "/50 gal");
            Console.WriteLine("     _________________________________________________");

            Console.WriteLine();
            Console.WriteLine(" ¿Que acción desea realizar?");
            Console.WriteLine();
            Console.WriteLine(" 1. Ingresar gasolina a los depositos.");
            Console.WriteLine(" 2. Definir el precio por galon de los combustibles.");
            Console.WriteLine(" 3. Volver al menu principal");
            Console.WriteLine();
            Console.Write(" Presione el numero de accion que desea realizar:  ");
            eleccion2 = Convert.ToInt32(Console.ReadLine());

            //ELECCION DE LA OPCION QUE VA A REALIZAR
                 switch (eleccion2)
                 {
                      case 1:
                        Console.Clear();
                        goto IngresarGasolina;
                      case 2:
                        Console.Clear();
                        goto DefinirPrecio;
                    case 3:
                        Console.Clear();
                        goto inicio;
                    default:
                        Console.WriteLine();
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error. Esa opción no es valida. ");
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.ReadLine();
                        Console.Clear();
                        goto intento;
                 }

            // PROCESO PARA INGRESAR LOS COMBUSTIBLES A LOS DEPOSITOS

        IngresarGasolina:

            int CantidadIngresar;

            Console.WriteLine();
            Console.WriteLine("     A elegido ingresar mas combustible a los depositos ");
            Console.WriteLine("     Por favor ingrese la cantidad de galones deseada para cada deposito.");
            Console.WriteLine("     Recuerde que  50 gal. es la máxima capacidad de los depositos.");

            Console.WriteLine();
            Console.Write("     Diesel (gal):  ");
            CantidadIngresar = Convert.ToInt32(Console.ReadLine());

            if ((CantidadIngresar + Diesel.CantidadCombustible) > 50)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" ERROR. La cantidad ingresada, sobrepasa la capacidad del deposito.");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" Presione cualquier tecla para continuar.");
                Console.WriteLine();
                goto Regular;
            }
            else
            {
                Diesel.CantidadCombustible += CantidadIngresar;
            }

            Console.Write("     Costo: Q. ");
            DDiesel.CostoIngresado = Convert.ToDouble(Console.ReadLine());
            Diesel1 = new clsCostoCombustible(DDiesel.Costo, (Diesel.CantidadCombustible - CantidadIngresar), CantidadIngresar, DDiesel.CostoIngresado);
            Console.ReadLine();


        Regular:
            //Console.WriteLine();
            Console.Write("     Regular (gal):  ");
            CantidadIngresar = Convert.ToInt32(Console.ReadLine());
            if ((CantidadIngresar + Regular.CantidadCombustible) > 50)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" ERROR. La cantidad ingresada, sobrepasa la capacidad del deposito.");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" Presione cualquier tecla para continuar.");
                Console.WriteLine();
                goto Super;
            }
            else
            {
                Regular.CantidadCombustible += CantidadIngresar;
            }

            Console.Write("     Costo: Q. ");
            DRegular.CostoIngresado = Convert.ToDouble(Console.ReadLine());
            Regular1  = new clsCostoCombustible(DRegular.Costo, (Regular.CantidadCombustible - CantidadIngresar), CantidadIngresar, DRegular.CostoIngresado);
            Console.ReadLine();

        Super:
            Console.WriteLine();
            Console.Write("     Super (gal):  ");
            CantidadIngresar = Convert.ToInt32(Console.ReadLine());
            if ((CantidadIngresar + Super.CantidadCombustible) > 50)
            {
                Console.WriteLine();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(" ERROR. La cantidad ingresada, sobrepasa la capacidad del deposito.");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(" Presione cualquier tecla para continuar.");
                Console.WriteLine();
                goto terminar;
            }
            else
            {
                Super.CantidadCombustible += CantidadIngresar;
            }

            Console.Write("     Costo: Q. ");
            DSuper.CostoIngresado = Convert.ToDouble(Console.ReadLine());

        //Console.WriteLine(" Costo Actual: " + Super1.CostoActual().ToString("#.##"));

        terminar:
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("             Los datos han sido guardados exitosamente");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(" Presione cualquier tecla para regresar al menu de administración.");
            Console.WriteLine(Super1.CostoActual().ToString("#.##"));
            Console.ReadLine();
            Console.Clear();

            

            goto Depositos;

        DefinirPrecio:





            goto Final;

        Ventas:
            Console.WriteLine();
            Console.WriteLine("         Venta de gasolina ");
            goto Final;

        Informacion:
            Console.WriteLine();
            Console.WriteLine("         Información Financiera");
            goto Final;

        Datos:
            Console.WriteLine();
            Console.WriteLine("         Manejo de Datos ");
            goto Final;


        Final:
            Console.ReadKey();
        }

    }
}

