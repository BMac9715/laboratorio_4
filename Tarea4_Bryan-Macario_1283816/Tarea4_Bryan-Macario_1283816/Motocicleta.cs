﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tarea4_Bryan_Macario_1283816
{
    class Motocicleta
    {
        private int _modelo;
        private double _precio;
        private string _marca;
        private double _iva;

        public Motocicleta(int modelo, double precio, string marca, double iva)
        {
            _modelo = modelo;
            _precio = precio;
            _marca = marca;
            _iva = iva;
        }

        public string showData()
        {
            string datos;
            datos = "   Marca: " + _marca;
            datos = "\n     Modelo: " + _modelo;
            datos = "\n     Precio: " + _precio;
            datos = "\n     Iva: " + _iva;

            return datos;       
        }
    }
}
