﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Bryan_Macario_1283816
{
    class Program
    {
        static void Main(string[] args)
        {
            Automovil Auto1 = new Automovil(2010, 10000.00, "", false , 7.75);

            Console.WriteLine("Por favor, Ingrese los datos que se le piden ingresar, de su automovil: ");
            Console.WriteLine();
            Console.WriteLine("Ingrese la marca del auto: ");
            Auto1.marca = Console.ReadLine();
            Console.WriteLine("Ingrese el modelo del auto:");
            Auto1.modelo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingrese el precio que cuesta:");
            Auto1.precio = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingrese el tipo de cambio a dolares:");
            Auto1.tipoCambioDolar = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("¿Esta disponible? (true o false): ");
            Auto1.disponible = Convert.ToBoolean (Console.ReadLine());

            Console.WriteLine();
            Console.WriteLine(" Datos del auto:");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(Auto1.MostrarInformaicon());

            Console.ReadKey();
        }
    }
}
