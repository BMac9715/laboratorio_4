﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_Bryan_Macario_1283816
{
    class Automovil
    {

        //Esta lista es de los atributos que tendrá la clase, llevan "private o public"
        //dependiendo si desea tenerlo disponible para todo, o solamente para esa clase.

        private int _modelo;
        private double _precio;
        private string _marca;
        private bool _disponible;
        private double _tipoCambioDolar;

        //Esta parte del codigo, se le llama constructor
        
        public  Automovil (int modelo, double precio, string marca, bool disponible, double tipoCambioDolar)
        {
            _modelo = modelo;
            _precio = precio;
            _marca = marca;
            _disponible = disponible;
            _tipoCambioDolar = tipoCambioDolar;
        }
        
        //Metodos para hacerlo publicos y que se puedan usar en el main.

        public int modelo
        {
            get
            {
                return _modelo;
            }
            set
            {
                _modelo = value;
            }

        }

        public double precio
        {
            get
            {
                return _precio;
            }
            set
            {
                _precio = value;
            }
        }

        public string marca
        {
            get
            {
                return _marca;
            }
            set
            {
                _marca = value;
            }
        }

        public double tipoCambioDolar
        {
            get
            {
                return _tipoCambioDolar;
            }
            set
            {
                _tipoCambioDolar = value;
            }
        }

        public bool disponible
        {
            get
            {
                return _disponible;
            }
            set
            {
                _disponible = value;
            }
        }

        public string MostrarDisponibilidad()
        {
            if (_disponible == true)
            {
                return "Disponible";
            }
            else
            {
                return "No se encuentra disponible actualmente";
            }
        }

        public string MostrarInformaicon()
        {
            string resultado;
            resultado = "     Marca: " + marca;
            resultado += "\n     Modelo: " + modelo ;
            resultado += "\n     Precio: Q." + precio.ToString();
            resultado += "\n     Precio en dolares: $" + (precio / tipoCambioDolar).ToString("#.##");
            resultado += "\n     " + MostrarDisponibilidad();

            return resultado;
        }
    }
}
